package cargo.work;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class TestWorkingdayornot {

	@Test
	public void TestWorkingday() {
		Cargo c=new Cargo("sowmi",LocalDate.now(),LocalTime.now(),11);
		assertTrue(LocalDate.of(2020, 1, 16).equals(c.CheckWorkingdayrnot(LocalDate.of(2020, 1, 15))));//public holiday
		assertTrue(LocalDate.of(2020, 1, 13).equals(c.CheckWorkingdayrnot(LocalDate.of(2020, 1, 11))));//sat 
		assertTrue(LocalDate.of(2020, 1, 20).equals(c.CheckWorkingdayrnot(LocalDate.of(2020, 1, 19))));//sunday
		
	}
}
