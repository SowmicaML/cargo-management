package cargo.work;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class CargoDemo {
	public static void main(String[] args) {
		Cargo c1=new Cargo("Grady Booch",LocalDate.now(),LocalTime.now(),24);
		c1.EstimateDelivery();
		Cargo c2=new Cargo("Eric Gamma",LocalDate.now(),LocalTime.now(),11);
		c2.EstimateDelivery();
	}
}

class Cargo{
	//enum for public holidays
	enum publicholidays{
		NEWYEAR(LocalDate.of(LocalDate.now().getYear(), 01, 01)),
		REPUBLICDAY(LocalDate.of(LocalDate.now().getYear(), 01, 15));
		
		private LocalDate ld;
		
		publicholidays(LocalDate ld) {
			this.ld=ld;
		}
		LocalDate getlocaldate() {
			return this.ld;
		}
	}
	
	String CustomerName;
	LocalDate Shipmentdate;
	LocalDate Deliverydate;
	LocalTime time;
	LocalTime Dtime;
	float NoOfHours;
	
	//cargo delivery is from 6AM to 6Pm(18:00:00)
	LocalTime end= LocalTime.of(18,0,0);
	
	//Parameterized constructor for initializing the details
	public Cargo(String name,LocalDate d,LocalTime t,int hrs) {
		this.CustomerName=name;
		this.Shipmentdate=d;
		this.time=t;
		this.NoOfHours=hrs;
	}
	
	//method that estimates the parcels's delivery date and time
	public void EstimateDelivery() {
		
		LocalDate d=Shipmentdate;
		LocalTime t=time;
		float h=NoOfHours;
		while(h>0) {
			d=CheckWorkingdayrnot(d);
			
			Duration duration = Duration.between(t, end);
			t=LocalTime.of(6,0,0);
			if(h<=duration.toHours()) {
				t=t.plusSeconds((long) (h*3600));
			}
			h-=(float)(duration.getSeconds())/3600;
			if(h>0)
				d=d.plus(1,ChronoUnit.DAYS);
			
		}
		Deliverydate=d;
		Dtime=t;
		PrintDetails();
	}
	
	//method to print details
	public void PrintDetails() {
		System.out.println("Customer Name   :"+CustomerName+"\n"+"NO.of hours for delivery  :"
		+NoOfHours+"Hours"+"\n"+"Ordered on  :"+Shipmentdate+"\n"+"Ordered at  :"+time);
		System.out.println("Estimated delivery date: "+fromLocalDateInUTC(Deliverydate));
		System.out.println("Estimated delivery time: "+Dtime);
		System.out.println();
	}
	
	//method to convert LocalTime to UTC
	public Date fromLocalDateInUTC(LocalDate date) {
	    return Date.from(date.atStartOfDay().toInstant(ZoneOffset.UTC));
	  }
	
	//method to check if the day is working or not
	public LocalDate CheckWorkingdayrnot(LocalDate d) {
		if(d.getDayOfWeek().getValue()==6 ) 
			d=d.plus(1,ChronoUnit.DAYS);
		if(d.getDayOfWeek().getValue()==7) 
			d=d.plus(1,ChronoUnit.DAYS);
		
		for(publicholidays ph:publicholidays.values()) {
			if(d.equals(ph.getlocaldate()))
				d=d.plus(1,ChronoUnit.DAYS);
		}
		return d;
	}
}