package cargo.work;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses( {TestEstimateDelivery.class ,  TestWorkingdayornot.class})
public class TestSuiteclass {

}