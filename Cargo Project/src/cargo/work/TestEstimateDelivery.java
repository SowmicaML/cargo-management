package cargo.work;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class TestEstimateDelivery {
	@Test
	public void TestDelivery() {
		Cargo c=new Cargo("sowmi",LocalDate.now(),LocalTime.of(11,0),11);
		c.EstimateDelivery();
		assertTrue((LocalDate.of(2020, 9, 23).equals(c.Deliverydate)));
		assertTrue((LocalTime.of(10, 0).equals(c.Dtime)));
		
		Cargo c1=new Cargo("ramu",LocalDate.now(),LocalTime.of(11,0),24);
		c1.EstimateDelivery();
		assertTrue((LocalDate.of(2020, 9, 24).equals(c1.Deliverydate)));
		assertTrue((LocalTime.of(11, 0).equals(c1.Dtime)));
		
		Cargo c3=new Cargo("somu",LocalDate.now(),LocalTime.of(11,30),11);
		c3.EstimateDelivery();
		assertTrue((LocalDate.of(2020, 9, 23).equals(c3.Deliverydate)));
		assertTrue((LocalTime.of(10, 30).equals(c3.Dtime)));
	}
}
